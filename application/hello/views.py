# -*- coding: utf-8 -*-
import logging

from flask import make_response
from flask import render_template
from flask import request, Blueprint
from google.appengine.ext.blobstore import blobstore
from werkzeug.http import parse_options_header

bp_hello = Blueprint('hello', __name__, url_prefix='/hello')


@bp_hello.route('/', methods=['GET'])
def home():
    # Blobstore に保存されているファイルを取得
    files = blobstore.BlobInfo.all()
    return render_template('index.html', files=files)


@bp_hello.route('/upload', methods=['GET'])
def upload():
    upload_url = blobstore.create_upload_url('/hello/upload_photo')
    logging.info(upload_url)
    return render_template('upload.html', upload_url=upload_url)


@bp_hello.route("/upload_photo", methods=['POST'])
def upload_file():
    if request.method == 'POST':

        # File取得
        uploaded_file = request.files['file']
        header = uploaded_file.headers['Content-Type']

        # blob-key取得
        parsed_header = parse_options_header(header)
        blob_key = parsed_header[1]['blob-key']

        return render_template('uploaded_file_link.html', blob_key=blob_key)


@bp_hello.route("/view_photo/<blob_key>", methods=['GET', 'POST'])
def view_photo(blob_key=''):
    blob_info = blobstore.get(blob_key)
    response = make_response(blob_info.open().read())
    response.headers['Content-Type'] = blob_info.content_type
    return response
